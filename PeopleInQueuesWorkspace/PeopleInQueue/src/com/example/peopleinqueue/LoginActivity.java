package com.example.peopleinqueue;

import java.util.Arrays;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;

/**
 * Activity which displays a login screen to the user, offering registration as
 * well.
 */
public class LoginActivity extends Activity {

	private static final String TAG = "LoginActivity";
	private UiLifecycleHelper uiHelper;
	private Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state, Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		initLogin();
		uiHelper = new UiLifecycleHelper(this, callback);
		uiHelper.onCreate(savedInstanceState);


	}

	private void onSessionStateChange(Session session, SessionState state, Exception exception) {
		if (state.isOpened()) {
			Log.i(TAG, "Logged in...");
			launchMainWithUsernameActivity(session);
			
		} else if (state.isClosed()) {
			Log.i(TAG, "Logged out...");
		}
	}



	@SuppressWarnings("deprecation")
	private void launchMainWithUsernameActivity(Session session) {
		final String un = "";
		Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {

			// callback after Graph API response with user object
			@Override
			public void onCompleted(GraphUser user, Response response) {

				if (user != null) {

					toast("Hello " + user.getName() + "!");
					Log.d("facebook", "Hello " + user.getName() + "!");
					launchMainActivity(user.getName());

				}
			}


		});
	}

	

	private void toast(String msg) {

		Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
	}


	private void initLogin() {
		setContentView(R.layout.activity_login);

		View signInButton = findViewById(R.id.sign_in_button);
		signInButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				launchMainActivity(getUsernameFromScreen());

			}

			private String getUsernameFromScreen() {

				EditText userNameView = (EditText) findViewById(R.id.email);
				
				return userNameView.getText().toString();
			}

		});
		
		LoginButton authButton = (LoginButton) findViewById(R.id.authButton);
		authButton.setReadPermissions(Arrays.asList("user_likes", "user_status"));

	}

	private void launchMainActivity(String username) {
		
		Intent intent = new Intent(LoginActivity.this, MainActivity.class);
		intent.putExtra("username", username);
		startActivity(intent);
		finish();				
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		uiHelper.onResume();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		 uiHelper.onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		uiHelper.onPause();
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		uiHelper.onDestroy();
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}
	
	
	private void startFaceLogin() {
		Session.openActiveSession(this, true, new Session.StatusCallback() {

			// callback when session changes state
			@SuppressWarnings("deprecation")
			@Override
			public void call(Session session, SessionState state, Exception exception) {

				if (session.isOpened()) {

					// make request to the /me API
					Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {

						// callback after Graph API response with user object
						@Override
						public void onCompleted(GraphUser user, Response response) {

							if (user != null) {

								initLogin();

								toast("Hello " + user.getName() + "!");
								Log.d("facebook", "Hello " + user.getName() + "!");
								//								username = user.getName();
								//								handler = new Handler();
								//
								//								ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
								//								viewPager.setAdapter(new MyPagerAdapter());
								//
								//								getData();
							}
						}



					});

				}

			}
		});
	}
}
