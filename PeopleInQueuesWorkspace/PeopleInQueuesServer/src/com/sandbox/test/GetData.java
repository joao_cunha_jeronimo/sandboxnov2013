package com.sandbox.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class GetData
 */
public class GetData extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public GetData() {
    	
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.getWriter().println(getFromFile());
	}

	private synchronized String getFromFile() {
		
		StringBuilder stringBuilder;
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File("WebContent/WEB-INF/data.txt")));
		    String line = null;
		    stringBuilder = new StringBuilder();
	
		    while((line = reader.readLine()) != null ) {
		        stringBuilder.append(line);
		    }
		    
		    reader.close();
		}
		catch(Exception e) {
			
			e.printStackTrace();
			return "";
		}

	    return stringBuilder.toString();
	}
}
