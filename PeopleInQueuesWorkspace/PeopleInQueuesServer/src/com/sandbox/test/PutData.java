package com.sandbox.test;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Servlet implementation class PutData
 */
public class PutData extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public PutData() {
        
    }
    
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if (!request.getParameterMap().containsKey("post")
				|| !request.getParameterMap().containsKey("poster")) {
			
			response.getWriter().println("you need to send post and poster");
		}
		else {
			
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			 Calendar cal = Calendar.getInstance();
			
			saveToFile(request.getParameter("post") + ", "
					+ request.getParameter("poster") + ", " + dateFormat.format(cal.getTime()));
		}
	}
	
	private synchronized void saveToFile(String string) {
		
		 try {
			 FileWriter fstream = new FileWriter("WebContent/WEB-INF/data.txt");
			 BufferedWriter out = new BufferedWriter(fstream);
			 
			 out.write(string);
			 out.close();
		 }
		 catch (Exception e) {
			 e.printStackTrace();
		 }
	}

}
